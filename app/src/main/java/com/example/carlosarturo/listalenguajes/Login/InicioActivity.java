package com.example.carlosarturo.listalenguajes.Login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carlosarturo.listalenguajes.R;

public class InicioActivity extends AppCompatActivity {
    Bundle extra;
    TextView txtNombre;
    TextView txtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        extra=this.getIntent().getExtras();
        txtNombre = (TextView) findViewById(R.id.inicioActivityTxtNombre);
        txtPass=(TextView) findViewById(R.id.inicioActivityTxtPass);
        txtNombre.setText(extra.getString("user"));
        txtPass.setText(extra.getString("pass"));
    }
}
