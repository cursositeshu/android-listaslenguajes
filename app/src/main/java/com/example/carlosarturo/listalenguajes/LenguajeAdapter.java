package com.example.carlosarturo.listalenguajes;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by CarlosArturo on 08/06/2016.
 */
public class LenguajeAdapter extends ArrayAdapter<Lenguaje> {

    Context contex;
    int LayoutResourceId;
    Lenguaje datos[]=null;

    public LenguajeAdapter(Context contex, int LayoutResorceId, Lenguaje[] datos){
        super(contex,LayoutResorceId, datos);

        this.contex=contex;
        this.LayoutResourceId=LayoutResorceId;
        this.datos=datos;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View row=convertView;
        LenguajesHolder holder=null;

        if(row==null){
            LayoutInflater inflater=((Activity)contex).getLayoutInflater();
            row=inflater.inflate(LayoutResourceId, parent, false);
            holder =new LenguajesHolder();

            holder.imagen=(ImageView) row.findViewById(R.id.imagen);
            holder.texto=(TextView) row.findViewById(R.id.tv);
            row.setTag(holder);
        }else{
            holder=(LenguajesHolder) row.getTag();
        }

        Lenguaje lenguaje=datos[position];
        holder.texto.setText(lenguaje.titulo);
        holder.imagen.setImageResource(lenguaje.icon);
        return  row;

    }

    static class LenguajesHolder{
        ImageView imagen;
        TextView texto;
    }
}
