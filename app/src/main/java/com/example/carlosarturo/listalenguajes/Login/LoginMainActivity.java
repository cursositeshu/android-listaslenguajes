package com.example.carlosarturo.listalenguajes.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.carlosarturo.listalenguajes.R;



/**
 * Created by CarlosArturo on 09/06/2016.
 */



public class LoginMainActivity extends AppCompatActivity {

    private EditText txtName;
    private EditText txtPass;
    private Button btnSubmint;
    private Button btnCrearCuenta;
    private Button btnCambiarPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        txtName=(EditText) findViewById(R.id.txtUser);
        txtPass=(EditText) findViewById(R.id.txtPass);
        btnSubmint=(Button) findViewById(R.id.btnLogin);
        btnCrearCuenta=(Button) findViewById(R.id.btnCrearCuenta);
        btnCambiarPass=(Button) findViewById(R.id.btnCambiarPass);




        btnCambiarPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),RestorePassword.class);
                startActivity(intent);
            }
        });

        btnCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CrearCuenta.class);
                startActivity(intent);
            }
        });


        btnSubmint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre= txtName.getText().toString();
                String pass=txtPass.getText().toString();
                //Toast.makeText(getApplicationContext(),nombre + ": " + pass,Toast.LENGTH_SHORT).show();
                //TODO: buscar que es un fragmento
                //TODO: que es SQLite y las clases necesarias para usarlo en android
                Intent intent=new Intent(getApplicationContext(),InicioActivity.class);
                intent.putExtra("user",nombre);
                intent.putExtra("pass",pass);
                startActivity(intent);
            }
        });

    }
}
