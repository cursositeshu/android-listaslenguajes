package com.example.carlosarturo.listalenguajes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView ls;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Lenguaje lenguajes[]=new Lenguaje[]{
                new Lenguaje(R.drawable.arduino,"Processing"),
                new Lenguaje(R.drawable.cobol,"Cobol"),
                new Lenguaje(R.drawable.csharp,"C#"),
                new Lenguaje(R.drawable.java,"Java"),
                new Lenguaje(R.drawable.javascript,"JavaScript"),
                new Lenguaje(R.drawable.pascal,"Pascal"),
                new Lenguaje(R.drawable.php,"PHP"),
                new Lenguaje(R.drawable.python,"Python"),
                new Lenguaje(R.drawable.rubi,"Rubi"),
                new Lenguaje(R.drawable.vbnet,"VB.Net")
        };
        //al adaptar se le pasa el arreglo lleno
        LenguajeAdapter adapter=new LenguajeAdapter(this,R.layout.item_row,lenguajes);
        //se trae el contenido del array lleno
        ls=(ListView) findViewById(R.id.lsLenguajes);
        ls.setAdapter(adapter);

        ls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView v=(TextView) view.findViewById(R.id.tv);
                Toast.makeText(getApplicationContext(),v.getText(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
