package com.example.carlosarturo.listalenguajes;

/**
 * Created by CarlosArturo on 08/06/2016.
 */
//Contiene la estructura de los elementos de la lista
public class Lenguaje {
    public int icon;
    public String titulo;

    public Lenguaje()
    {
        super();
    }

    public Lenguaje(int icon, String titulo){
        super();
        this.icon=icon;
        this.titulo=titulo;
    }
}
